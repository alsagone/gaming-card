/* eslint-disable */
import Vue from 'vue'
import VueFontAwesomeCss from 'vue-fontawesome-css'
import VueHtml2Canvas from 'vue-html2canvas'

Vue.use(VueFontAwesomeCss)
Vue.use(VueHtml2Canvas)