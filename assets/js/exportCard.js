/* eslint-disable */
var exportCard = {
    export () {
        const element = document.getElementById('#card')

        const options = {
            type: 'dataURL'
        }
        const image = $html2canvas(element, options)

        // Open the image in a new window
        window.open(image, '_blank')
    }
}

export default exportCard